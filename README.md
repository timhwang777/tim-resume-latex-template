# Tim's Resume with LaTeX

![static-badge](https://img.shields.io/badge/LaTeX-Solutions-blue?logo=LaTeX
)
## Table of Contents
1. [About the Project](#about-the-project)
2. [Getting Started](#getting-started)
3. [Author](#author)

## About the Project
A LaTeX resume template modified from [Awesome-CV](https://github.com/posquit0/Awesome-CV).

I added some functions in the class files to make the colors less vivid and to only emphasize the sections. You can turn the color rendering on or off by calling the relative function in `resume.tex`.

This repository is a storage for my LaTeX template. I have already moved from LaTeX to [Typst](https://typst.app/about/), a new tool aiming to substitute LaTeX. It is fast, lighter, and less dependent. Although LaTeX is still pretty neat for most of the work in academia, it is too hard to maintain.

## Getting Started
You can work with this template in one of two ways:
1. Use the Overleaf online LaTeX IDE.
2. Use Visual Studio Code with LaTeX extensions, in combination with Tex Live.

## Author
Timothy Hwang
